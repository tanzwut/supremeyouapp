# Generated by Django 2.2.7 on 2020-12-21 15:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('supremeyouapi', '0012_auto_20201219_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ability',
            name='id_ability',
            field=models.CharField(default='be84b280-ac15-4023-94e3-d0a327ae0f43', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='habitalarm',
            name='id_habit_alarm',
            field=models.CharField(default='76008903-c17e-4671-a14c-ca87f7563c84', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='habitfrecuencydays',
            name='id_habit_frecuency_days',
            field=models.CharField(default='71717598-3aa0-4711-98da-b06a285ec42d', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='habito',
            name='id_tarea',
            field=models.CharField(default='210672e5-77cb-4f9c-bf77-88944b17b34e', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='habitocompletado',
            name='id_habito_completado',
            field=models.CharField(default='4c527519-3e8f-4a1e-bf11-6606385b1ac5', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='lista',
            name='id_lista',
            field=models.CharField(default='1f83409a-7d5f-45a7-aba0-3185aca523c1', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='meta',
            name='id_tarea',
            field=models.CharField(default='210672e5-77cb-4f9c-bf77-88944b17b34e', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='milestone',
            name='id_tarea',
            field=models.CharField(default='210672e5-77cb-4f9c-bf77-88944b17b34e', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='nota',
            name='id_nota',
            field=models.CharField(default='0acf7b40-7dd1-457b-bdf1-b246a7354adb', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='reward',
            name='id_reward',
            field=models.CharField(default='af482a1f-fd0e-4d67-a169-a9a0e5037cb5', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='tarea',
            name='id_tarea',
            field=models.CharField(default='210672e5-77cb-4f9c-bf77-88944b17b34e', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='trigger',
            name='id_trigger',
            field=models.CharField(default='ab84b699-980c-4512-90b9-a95378c77afc', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='triggerevent',
            name='id_trigger_event',
            field=models.CharField(default='1a0d0e74-174b-4e70-9a95-ab970b55e355', editable=False, max_length=150, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='focusArea',
            field=models.CharField(blank=True, default=None, max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='motivationalQuote',
            field=models.CharField(blank=True, default=None, max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='personalVisionStatement',
            field=models.CharField(blank=True, default=None, max_length=150, null=True),
        ),
    ]
