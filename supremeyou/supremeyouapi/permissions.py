from rest_framework import exceptions
from rest_framework.permissions import BasePermission, SAFE_METHODS
from django.utils.translation import gettext_lazy as _
from .models import Usuario


class IsLoggedInUserOrAdmin(BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj == request.user or request.user.is_staff


class IsAdminUser(BasePermission):

    def has_permission(self, request, view):
        return request.user and request.user.is_staff

    def has_object_permission(self, request, view, obj):
        return request.user and request.user.is_staff


class IsSelf(BasePermission):
    def has_object_permission(self, request, view, obj):
        if isinstance(obj, Usuario):
            return request.user == obj
        raise exceptions.PermissionDenied(
            detail=_('Received object of wrong instance'), code=403)
