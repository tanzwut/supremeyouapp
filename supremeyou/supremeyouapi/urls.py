from os import name
from django.urls import include, path
from rest_framework import routers
from . import views
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r'Usuarios', views.UsuarioViewSet, basename='Usuarios')
ROUTER.register(r'Secciones', views.SeccionViewSet)
ROUTER.register(r'Encuestas', views.EncuestaViewSet)
ROUTER.register(r'Cuestionarios', views.CuestionarioViewSet)
ROUTER.register(r'Preguntas', views.PreguntasViewSet)
ROUTER.register(r'RespuestaUsuario', views.RespuestaUsuarioViewSet)
ROUTER.register(r'Respuestas', views.RespuestasViewSet)
#ROUTER.register(r'Countries', views.CountryViewSet, base_name='Countries')
ROUTER.register(r'Relaciones', views.RelationshipsViewSet)
ROUTER.register(r'Carreras', views.CareersViewSet)
ROUTER.register(r'Tareas', views.TareaViewset, basename='Tareas')
ROUTER.register(r'Metas', views.MetaViewset, basename='Metas')
ROUTER.register(r'Milestones', views.MilestoneViewset, basename='Milestones')
ROUTER.register(r'Habitos', views.HabitoViewset, basename='Habitos')
ROUTER.register(r'HabitFrecuencyDays',
                views.HabitFrecuencyDaysViewset, basename='HabitFrecuencyDays')
ROUTER.register(r'HabitoCompletado',
                views.HabitoCompletadoViewset, basename='HabitoCompletado')
ROUTER.register(r'HabitAlarms', views.HabitAlarmViewSet,
                basename='HabitAlarms')
ROUTER.register(r'Triggers', views.TriggerViewset, basename='Triggers')
ROUTER.register(r'TriggerEvents', views.TriggerEventViewset,
                basename='TriggerEvents')
ROUTER.register(r'Rewards', views.RewardViewset, basename='Rewards')
ROUTER.register(r'Abilities', views.AbilityViewset, basename='Abilities')
ROUTER.register(r'Notas', views.NotaViewset, basename='Notas')
ROUTER.register(r'Listas', views.ListaViewset, basename='Listas')
ROUTER.register(r'SyncData',
                views.SyncDataAPIViewSet, basename='SyncData')
ROUTER.register(r'device/apns', views.APNSSecureViewSet)
ROUTER.register(r'TenAreas', views.TenAreasViewSet, basename='TenAreas')
ROUTER.register(r'UserPurchase', views.UserPurchaseViewSet,
                basename='UserPurchase')
ROUTER.register(r'PromotionalCode', views.PromotionalCodeViewSet,
                basename='PromotionalCode')
ROUTER.register(r'Stages', views.StageViewSet)
ROUTER.register(r'StageHabitos', views.StageHabitoViewSet,
                basename='StageHabitos')
ROUTER.register(r'Badges', views.BadgeViewSet)
ROUTER.register(r'BadgeHabitos', views.BadgeHabitoViewSet,
                basename='BadgeHabitos')

urlpatterns = [
    path('', include(ROUTER.urls)),
    #path('auth/', include('rest_auth.urls')),
    path('rest-auth/', include('rest_framework.urls')),
    # path('token/', views.CustonTokenObtainPairView.as_view(),
    #    name='token_obtain_pair'),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provier')),
    path('sync/', views.SyncTriggerView.as_view(), name='sync'),
    path('password_reset/',
         include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('change-password/', views.change_password, name='changePassword'),
    path('verifyReceipt/', views.verifyReceipt, name='verifyReceipt'),
    path('generateSignature/', views.generate_signature, name='generateSignature'),
    path('focusOnPastOrFuture/', views.addFocusOnPastOrFuture,
         name='focusOnPastOrFuture'),
    path('turnYourLife/', views.addTurnYourLife, name='turnYourLife'),
    path('affectsYouMost/', views.addAffectsYouMost, name='afffectsYouMost')
]
