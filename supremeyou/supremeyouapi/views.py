import base64
from hashlib import sha256
import hashlib
import os
import time
from django.http.response import JsonResponse
from ecdsa.util import sigencode_der
import requests
import json
import environ
import calendar
import uuid
import ecdsa
from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy, reverse
from django.dispatch import receiver
from django.shortcuts import redirect
from .forms import CustomUserCreationForm
from rest_framework.response import Response
from rest_framework.decorators import action, api_view, permission_classes

from rest_framework import viewsets, permissions, request, status, generics

from .serializers import AffectsYouMostSerializer, BadgeHabitoSerializer, BadgeSerializer, CountrySerializer, FocusOnPastOrFutureSerializer, PromotionalCodeSerializer, StageHabitoSerializer, StageSerializer, TenAreasSerializer, TurnYourLifeSerializer, UserPurchaseSerializer, UsuarioSerializer, SeccionSerializer, EncuestaSerializer, CuestionarioSerializer, PreguntasSerializer, RespuestaUsuarioSerializer, RespuestasSerializer, RelationshipsSerializer, CareersSerializers, MilestoneSerializer, HabitoSerializer, HabitFrecuencyDaysSerializer, HabitoCompletadoSerializer, HabitAlarmSerializer, TriggerSerializer, TriggerEventSerializer, ReawardSerializer, AbilitySerializer, ListaSerializer, TareaSerializer, MetaSerializer, SyncTriggerSerializer, UserChangePasswordSerializer, NotaSerializer
from .models import Badge, BadgeHabito, Nota, PromotionalCode, Stage, StageHabito, TenAreas, UserPurchase, Usuario, Seccion, Encuesta, Cuestionario, Preguntas, RespuestaUsuario, Respuestas, Relationships, Careers, Milestone, Habito, HabitFrecuencyDays, HabitoCompletado, HabitAlarm, Trigger, TriggerEvent, Reward, Ability, Lista, Tarea, Meta, SyncInfo, Nota
from .permissions import IsLoggedInUserOrAdmin, IsAdminUser, IsSelf
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
# from django_countries import countries
from drf_multiple_model.viewsets import ObjectMultipleModelAPIViewSet
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet
from django.db import models
from push_notifications.models import APNSDevice
from datetime import datetime, timedelta, timezone
from django.shortcuts import get_object_or_404

from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import EmailMultiAlternatives
from django.core.files.storage import default_storage

# Create your views here.


class HomePageView(TemplateView):
    template_name = 'home.html'


def home_view(request):
    return redirect('http://web.supremeyou.co', permanent=True)


def awarness_video_view(request, video):
    url = default_storage.url('videos/%s' % (video,))
    return redirect(url, permanent=True)


def face_to_ios(request):
    return redirect('https://apps.apple.com/mx/app/supreme-you-goals-mindset/id1520753334', permanent=True)


class SignUp(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


"""class CustonTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer"""


class UsuarioViewSet(viewsets.ModelViewSet):
    #queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer

    def get_queryset(self):
        usuario = self.request.user

        return Usuario.objects.filter(email=usuario.email)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [permissions.AllowAny]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update' or self.action == 'list':
            permission_classes = [
                IsLoggedInUserOrAdmin, TokenHasReadWriteScope, IsSelf]
        elif self.action == 'destroy':
            permission_classes = [IsAdminUser, TokenHasReadWriteScope]

        return [permission() for permission in permission_classes]

    @action(detail=True, methods=['put'])
    def change_password(self, request, pk=None):
        user = Usuario.objects.get(pk=pk)
        self.check_object_permissions(request, user)
        ser = UserChangePasswordSerializer(
            user, data=request.data, many=False, context={'user': request.user})
        ser.is_valid(raise_exception=True)
        user = ser.save()
        return Response(ser.data, status=status.HTTP_200_OK)


class SeccionViewSet(viewsets.ModelViewSet):
    queryset = Seccion.objects.all()
    serializer_class = SeccionSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class EncuestaViewSet(viewsets.ModelViewSet):
    queryset = Encuesta.objects.all()
    serializer_class = EncuestaSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class CuestionarioViewSet(viewsets.ModelViewSet):
    queryset = Cuestionario.objects.all()
    serializer_class = CuestionarioSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class PreguntasViewSet(viewsets.ModelViewSet):
    queryset = Preguntas.objects.all()
    serializer_class = PreguntasSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class RespuestaUsuarioViewSet(viewsets.ModelViewSet):
    queryset = RespuestaUsuario.objects.all()
    serializer_class = RespuestaUsuarioSerializer
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]


class RespuestasViewSet(viewsets.ModelViewSet):
    queryset = Respuestas.objects.all()
    serializer_class = RespuestasSerializer
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]


class CountryViewSet(viewsets.GenericViewSet):
    serializer_class = CountrySerializer
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request):
        serializer = CountrySerializer(many=True).data
        return Response(serializer)


class RelationshipsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Relationships.objects.all()
    serializer_class = RelationshipsSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, TokenHasScope]
    required_scopes = ['read']


class CareersViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Careers.objects.all()
    serializer_class = CareersSerializers
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, TokenHasScope]
    required_scopes = ['read']


class MilestoneViewset(viewsets.ModelViewSet):
    #queryset = Milestone.objects.all()
    serializer_class = MilestoneSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Milestone.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class HabitoViewset(viewsets.ModelViewSet):
    #queryset = Habito.objects.all()
    serializer_class = HabitoSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Habito.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class HabitFrecuencyDaysViewset(viewsets.ModelViewSet):
    #queryset = HabitFrecuencyDays.objects.all()
    serializer_class = HabitFrecuencyDaysSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return HabitFrecuencyDays.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class HabitoCompletadoViewset(viewsets.ModelViewSet):
    #queryset = HabitoCompletado.objects.all()
    serializer_class = HabitoCompletadoSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return HabitoCompletado.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class HabitAlarmViewSet(viewsets.ModelViewSet):
    #queryset = HabitAlarm.objects.all()
    serializer_class = HabitAlarmSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return HabitAlarm.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class TriggerViewset(viewsets.ModelViewSet):
    #queryset = Trigger.objects.all()
    serializer_class = TriggerSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Trigger.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class TriggerEventViewset(viewsets.ModelViewSet):
    #queryset = TriggerEvent.objects.all()
    serializer_class = TriggerEventSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return TriggerEvent.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class RewardViewset(viewsets.ModelViewSet):
    #queryset = Reward.objects.all()
    serializer_class = ReawardSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Reward.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class AbilityViewset(viewsets.ModelViewSet):
    #queryset = Ability.objects.all()
    serializer_class = AbilitySerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Ability.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class NotaViewset(viewsets.ModelViewSet):
    serializer_class = NotaSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Nota.objects.filter(usuario=user, tarea__eliminado=False, eliminado=False)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class ListaViewset(viewsets.ModelViewSet):
    #queryset = Lista.objects.all()
    serializer_class = ListaSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Lista.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class TareaViewset(viewsets.ModelViewSet):
    #queryset = Tarea.objects.all()
    serializer_class = TareaSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Tarea.objects.filter(usuario=user, eliminado=False)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class MetaViewset(viewsets.ModelViewSet):
    #queryset = Meta.objects.all()
    serializer_class = MetaSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return Meta.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class SyncDataAPIViewSet(ObjectMultipleModelAPIViewSet):
    # permission_classes = [
    #    permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_querylist(self):
        user_id = self.request.query_params['user']
        user = get_object_or_404(Usuario, pk=user_id)

        query_list = [
            {'queryset': Tarea.objects.filter(
                synced=False, usuario=user), 'serializer_class': TareaSerializer},
            {'queryset': Meta.objects.filter(
                synced=False, usuario=user), 'serializer_class': MetaSerializer},
            {'queryset': Milestone.objects.filter(
                synced=False, usuario=user), 'serializer_class': MilestoneSerializer},
            {'queryset': Habito.objects.filter(
                synced=False, usuario=user), 'serializer_class': HabitoSerializer},
            {'queryset': HabitFrecuencyDays.objects.filter(
                synced=False, usuario=user), 'serializer_class': HabitFrecuencyDaysSerializer},
            {'queryset': HabitoCompletado.objects.filter(
                synced=False, usuario=user), 'serializer_class': HabitoCompletadoSerializer},
            {'queryset': HabitAlarm.objects.filter(
                synced=False, usuario=user), 'serializer_class': HabitAlarmSerializer},
            {'queryset': Trigger.objects.filter(
                synced=False, usuario=user), 'serializer_class': TriggerSerializer},
            {'queryset': TriggerEvent.objects.filter(
                synced=False, usuario=user), 'serializer_class': TriggerEventSerializer},
            {'queryset': Reward.objects.filter(
                synced=False, usuario=user), 'serializer_class': ReawardSerializer},
            {'queryset': Ability.objects.filter(
                synced=False, usuario=user), 'serializer_class': AbilitySerializer},
            {'queryset': Lista.objects.filter(
                synced=False, usuario=user), 'serializer_class': ListaSerializer}
        ]

        return query_list


class APNSSecureViewSet(APNSDeviceAuthorizedViewSet):
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class SyncTriggerView(generics.GenericAPIView):
    serializer_class = SyncTriggerSerializer

    def get(self, request):
        now = datetime.now(tz=timezone.utc)

        try:
            last_sync = SyncInfo.objects.latest('synced_date')
            s_Date_Time = str(last_sync.synced_date.replace(microsecond=0))
            s_Date_Time = s_Date_Time[:-3]+s_Date_Time[-2:]
            pattern = '%Y-%m-%d %H:%M:%S%z'
            last_date_time = datetime.strptime(s_Date_Time, pattern)
            now = datetime.now(last_date_time.tzinfo)
            print(last_date_time)
            print(now)

            if now - last_date_time > timedelta(minutes=25):
                devices = APNSDevice.objects.all()

                for device in devices:
                    try:
                        device.send_message(None, content_available=1)
                    except Exception as e:
                        continue
                        return Response({'result': str(e), 'device': str(device.device_id)})

                result = {'result': 'Ok'}

                new_sync = SyncInfo.objects.create(synced_date=now)
            else:
                result = {'result': 'It\'s too soon to sync'}

            return Response(result)
        except SyncInfo.DoesNotExist:
            print('Not found')

        devices = APNSDevice.objects.all()

        for device in devices:
            device.send_message(None, content_available=1)

        result = {'result': 'Ok'}

        new_sync = SyncInfo.objects.create(synced_date=now)

        return Response(result)


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    msg = EmailMultiAlternatives(
        'Password reset from Supreme You',
        """
        Dear {user}.

        A password reset was requested. If you ask for this, click in the link below to reset your password.

        {url}?token={token}

        Best regards!
        """.format(user=reset_password_token.user.first_name, url=instance.request.build_absolute_uri(reverse('changePassword')), token=reset_password_token.key),
        'no-reply@supremeyou.co',
        [reset_password_token.user.email]
    )

    msg.send()


def change_password(request):
    token = request.GET['token']
    context = {'token': token}
    return render(request, 'change_password.html', context)


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated, TokenHasReadWriteScope])
def verifyReceipt(request):
    if request.method == 'POST':
        VERIFY_RECEIPT_SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt"
        VERIFY_RECEIPT_PROD = "https://buy.itunes.apple.com/verifyReceipt"
        env = environ.Env()
        env.read_env()

        request_body = {}

        request_body["receipt-data"] = request.data["receipt-data"]
        request_body["password"] = env('SHARED_SECRET')
        request_body['exclude-old-transactions'] = True

        receipt_data = json.dumps(request_body)

        verify_request = requests.post(VERIFY_RECEIPT_PROD, data=receipt_data)

        verify_result = verify_request.json()

        if verify_result['status'] == 21007:
            verify_request = requests.post(
                VERIFY_RECEIPT_SANDBOX, data=receipt_data)

            verify_result = verify_request.json()

            if verify_result['status'] == 0:
                return Response(verify_result, status=status.HTTP_200_OK)

        return Response(verify_result)


class TenAreasViewSet(viewsets.ModelViewSet):
    serializer_class = TenAreasSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return TenAreas.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class UserPurchaseViewSet(viewsets.ModelViewSet):
    serializer_class = UserPurchaseSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return UserPurchase.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


class PromotionalCodeViewSet(viewsets.ModelViewSet):
    serializer_class = PromotionalCodeSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return PromotionalCode.objects.filter(usuario=user)

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated, TokenHasReadWriteScope])
def generate_signature(request):
    if request.method == 'POST':
        env = environ.Env()
        env.read_env()

        file_path = os.path.join(
            settings.BASE_DIR, 'SubscriptionKey_S748429J7Y.pem')

        separator = '\u2063'

        productId = request.data['productIdentifier']
        offerId = request.data['offerIdentifier']
        appUsername = request.data['username']

        appBundleId = env('BUNDLE_ID')
        keyIdentifier = env('KEY_ID')
        nonce = str(uuid.uuid4())
        timestamp = str(int(round(time.time() * 1000)))

        combined_string = separator.join(
            [appBundleId, keyIdentifier, productId, offerId, appUsername, nonce, timestamp])

        try:
            with open(file_path, 'rb') as f:
                sk = ecdsa.SigningKey.from_pem(f.read())

            sig = sk.sign(combined_string.encode('utf-8'),
                          hashfunc=hashlib.sha256, sigencode=sigencode_der)

            base64_signature = base64.b64encode(sig)
        except Exception as ex:
            error = {'error': str(ex)}
            return JsonResponse(error, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        result = {}

        result['identifier'] = offerId
        result['keyIdentifier'] = keyIdentifier
        result['nonce'] = nonce
        result['signature'] = str(base64_signature, 'utf-8')
        result['timestamp'] = timestamp

        return JsonResponse(result, status=status.HTTP_200_OK)


class StageViewSet(viewsets.ModelViewSet):
    queryset = Stage.objects.all()
    serializer_class = StageSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]


class StageHabitoViewSet(viewsets.ModelViewSet):
    serializer_class = StageHabitoSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return StageHabito.objects.filter(usuario=user)

    def perform_create(self, serializer):
        return serializer.save(usuario=self.request.user)


class BadgeViewSet(viewsets.ModelViewSet):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class BadgeHabitoViewSet(viewsets.ModelViewSet):
    serializer_class = BadgeHabitoSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, TokenHasReadWriteScope]

    def get_queryset(self):
        user = self.request.user

        return BadgeHabito.objects.filter(usuario=user)

    def perform_create(self, serializer):
        return serializer.save(usuario=self.request.user)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def addFocusOnPastOrFuture(request):
    if request.method == 'POST':
        serializer = FocusOnPastOrFutureSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(request.method, status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def addTurnYourLife(request):
    if request.method == 'POST':
        serializer = TurnYourLifeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(request.method, status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def addAffectsYouMost(request):
    if request.method == 'POST':
        serializer = AffectsYouMostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(request.method, status=status.HTTP_405_METHOD_NOT_ALLOWED)
