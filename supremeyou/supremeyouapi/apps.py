from django.apps import AppConfig


class SupremeyouapiConfig(AppConfig):
    name = 'supremeyouapi'

    def ready(self):
        from . import signals
        return super().ready()
