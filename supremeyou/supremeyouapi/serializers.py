import uuid
from base64 import decode
from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import AffectsYouMost, Badge, BadgeHabito, FocusOnPastOrFuture, PromotionalCode, Stage, StageHabito, TenAreas, TurnYourLife, UserPurchase, Usuario, UserProfile, Categoria, Diario, Etiqueta, Meditacion, Meta, Nivel, NorthStar, Nota, Recurso, RitualMatutino, RitualNoche, Tarea, TipoCategoria, TipoMeditacion, Seccion, Preguntas, Encuesta, Cuestionario, Respuestas, RespuestaUsuario, Relationships, Careers, Milestone, Habito, HabitFrecuencyDays, HabitoCompletado, HabitAlarm, Trigger, TriggerEvent, Reward, Ability, Lista
#from django_countries.serializers import CountryFieldMixin
from django_countries.serializer_fields import CountryField
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.hashers import make_password
from django.core import exceptions
from django.contrib.auth.password_validation import validate_password as v_password
from datetime import datetime
from mailchimp_marketing import Client
from mailchimp_marketing.api_client import ApiClientError


class CountrySerializer(serializers.Serializer):
    country = CountryField(country_dict=True)


class SupremeBase64ImageField(serializers.ImageField):

    EMPTY_VALUES = (None, "", [], {}, ())

    def __init__(self, *args, **kwargs):
        self.reperesent_in_base64 = kwargs.pop('represent_in_base64', True)
        super(SupremeBase64ImageField, self).__init__(*args, **kwargs)

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        if data in self.EMPTY_VALUES:
            return None

        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')

            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid image')

            file_name = str(uuid.uuid4())
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

            return super(SupremeBase64ImageField, self).to_internal_value(data)

        raise exceptions.ValidationError(
            _("Invalid type. this is not a base64 string: {}".format(type(data))))

    def to_representation(self, value):
        from django.core.files.storage import default_storage
        import base64
        if self.reperesent_in_base64:
            if not value:
                return ""

            image = default_storage.open(value.name, 'rb').read()

            image64 = base64.b64encode(image).decode()

            return image64
        else:
            return super(SupremeBase64ImageField, self).to_representation(value)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class UserProfileSerializer(serializers.ModelSerializer):
    fecha_nacimiento = serializers.DateTimeField(
        required=False, input_formats=['%Y-%m-%d', ])
    profileImage = SupremeBase64ImageField(required=False)
    wellnessDone = serializers.BooleanField(required=False)
    fourteenDayChallenge = serializers.BooleanField(required=False)

    class Meta:
        model = UserProfile
        fields = ('id', 'fecha_nacimiento', 'profileImage',
                  'wellnessDone', 'fourteenDayChallenge')


class UsuarioSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(required=False)

    class Meta:
        model = Usuario
        fields = ('id', 'email', 'first_name',
                  'last_name', 'password', 'last_login', 'date_joined', 'profile')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        print(password)
        user = Usuario(**validated_data)
        if password is not None:
            user.set_password(password)
        user.save()

        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile

        #instance.email = validated_data.get('email', instance.email)
        # instance.save()

        profile.fecha_nacimiento = profile_data.get(
            'fecha_nacimiento', profile.fecha_nacimiento)
        profile.profileImage = profile_data.get(
            'profileImage', profile.profileImage)

        if profile_data.get('wellnessDone') is not None:
            profile.wellnessDone = profile_data.get(
                'wellnessDone', profile.wellnessDone)

        if profile_data.get('fourteenDayChallenge') is not None:
            profile.fourteenDayChallenge = profile_data.get(
                'fourteenDayChallenge', profile.fourteenDayChallenge)

        profile.save()

        return instance

    def validate_password(self, value: str) -> str:
        try:
            v_password(value, Usuario)
            return value
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages)


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id_categoria', 'nombre', 'id_tipo_categoria',
                  'fecha_creacion', 'fecha_modificacion')


class NorthStarSerializer(serializers.ModelSerializer):
    class Meta:
        model = NorthStar
        fields = ('id_north_star', 'desc_corta', 'desc_extendida',
                  'id_usuario', 'fecha_creacion', 'fecha_modificacion')


class NivelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nivel
        fields = ('id_nivel', 'nombre', 'nivel',
                  'fecha_creacion', 'fecha_modificacion')


class EtiquetaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Etiqueta
        fields = ('id_etiqueta', 'nombre',
                  'fecha_creacion', 'fecha_modifciacion')


class TipoMeditacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoMeditacion
        fields = ('id_tipo_meditacion', 'nombre',
                  'fecha_creacion', 'fecha_modificacion')


class TipoCategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoCategoria
        fields = ('id_tipo_categoria', 'nombre',
                  'fecha_creacion', 'fecha_modificacion')


class RecursoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recurso
        fields = ('id_recurso', 'titulo', 'descripcion', 'imagen', 'video',
                  'audio', 'id_etiqueta', 'fecha_creacion', 'fecha_modificacion')


class MeditacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meditacion
        fields = ('id_meditacion', 'id_tipo_meditacion',
                  'id_recurso', 'fecha_creacion', 'fecha_modificacion')


class RitualMatutinoSerializer(serializers.ModelSerializer):
    model = RitualMatutino
    fields = ('id_ritual_matutino', 'id_north_star', 'id_recurso',
              'id_meditacion', 'audio_exito', 'fecha_creacion', 'fecha_modificacion')


class RitualNocheSerializer(serializers.ModelSerializer):
    class Meta:
        model = RitualNoche
        fields = ('id_ritual_noche', 'id_north_star', 'id_tarea',
                  'audio_exito', 'fecha_creacion', 'fecha_modificacion')


class DiarioSerializer(serializers.ModelSerializer):
    class Meta:
        models = Diario
        fields = ('id_diario', 'id_ritual_matutino', 'id_ritual_noche',
                  'fecha_creacion', 'fecha_modificacion')


class NotaSerializer(serializers.ModelSerializer):
    id_nota = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Nota
        fields = '__all__'


class SeccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seccion
        fields = ('id_seccion', 'seccion',
                  'fecha_creacion', 'fecha_modificacion')


class PreguntasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Preguntas
        fields = ('id_pregunta', 'pregunta', 'id_seccion',
                  'fecha_creacion', 'fecha_modificacion')


class EncuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Encuesta
        fields = ('id_encuesta', 'descripcion', 'id_seccion',
                  'fecha_creacion', 'fecha_modificacion')


class CuestionarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cuestionario
        fields = ('id', 'id_encuesta', 'id_preguntas',
                  'fecha_creacion', 'fecha_modificacion')


class RespuestasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respuestas
        fields = ('id_respuestas', 'respuesta_desc', 'valor',
                  'fecha_creacion', 'fecha_modificacion')


class RespuestaUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = RespuestaUsuario
        fields = ('id', 'id_usuario', 'id_cuestionario', 'id_respuestas',
                  'fecha_creacion', 'fecha_modificacion')


"""class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        data['user'] = UsuarioSerializer(instance=self.user, context={
                                         'request': self.context.get('request')}).data

        return data"""


class RelationshipsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Relationships
        fields = ('id', 'relacion', 'fecha_creacion', 'fecha_modificacion')


class CareersSerializers(serializers.ModelSerializer):
    class Meta:
        model = Careers
        fields = ('id', 'career', 'fecha_creacion', 'fecha_modificacion')


class SyncTriggerSerializer(serializers.Serializer):
    result = serializers.CharField(read_only=True)


class MetaSerializer(serializers.ModelSerializer):
    id_tarea = serializers.CharField(default=str(uuid.uuid4()))
    image = SupremeBase64ImageField(required=False)

    class Meta:
        model = Meta
        fields = '__all__'


class TareaSerializer(serializers.ModelSerializer):
    id_tarea = serializers.CharField(default=str(uuid.uuid4()))
    image = SupremeBase64ImageField(required=False)

    class Meta:
        model = Tarea
        fields = '__all__'


class MilestoneSerializer(serializers.ModelSerializer):
    id_tarea = serializers.CharField(default=str(uuid.uuid4()))
    image = SupremeBase64ImageField(required=False)

    class Meta:
        model = Milestone
        fields = '__all__'


class HabitoSerializer(serializers.ModelSerializer):
    id_tarea = serializers.CharField(default=str(uuid.uuid4()))
    image = SupremeBase64ImageField(required=False)

    class Meta:
        model = Habito
        fields = '__all__'


class HabitFrecuencyDaysSerializer(serializers.ModelSerializer):
    id_habit_frecuency_days = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = HabitFrecuencyDays
        fields = '__all__'


class HabitoCompletadoSerializer(serializers.ModelSerializer):
    id_habito_completado = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = HabitoCompletado
        fields = '__all__'


class HabitAlarmSerializer(serializers.ModelSerializer):
    id_habit_alarm = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = HabitAlarm
        fields = '__all__'


class TriggerSerializer(serializers.ModelSerializer):
    id_trigger = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Trigger
        fields = '__all__'


class TriggerEventSerializer(serializers.ModelSerializer):
    id_trigger_event = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = TriggerEvent
        fields = '__all__'


class ReawardSerializer(serializers.ModelSerializer):
    id_reward = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Reward
        fields = '__all__'


class AbilitySerializer(serializers.ModelSerializer):
    id_ability = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Ability
        fields = '__all__'


class ListaSerializer(serializers.ModelSerializer):
    id_lista = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Lista
        fields = '__all__'


class UserChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True, required=True)
    new_password1 = serializers.CharField(write_only=True, required=True)
    new_password2 = serializers.CharField(write_only=True, required=True)

    def validate_old_password(self, value):
        user = self.context['user']
        if not user.check_password(value):
            raise serializers.ValidationError(
                _('Your old password was entered incorrectly, please enter it again'))
        return value

    def validate(self, data):
        if data['new_password1'] != data['new_password2']:
            raise serializers.ValidationError(
                {'new_password2': _('The two password fields didn\'t match')})
        v_password(data['new_password1'], self.context['user'])
        return data

    def save(self, **kwargs):
        password = self.validated_data['new_password1']
        user = self.context['user']
        user.set_password(password)
        user.save()
        return user


class TenAreasSerializer(serializers.ModelSerializer):
    id_ten_areas = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = TenAreas
        fields = ('id_ten_areas', 'introCompleted', 'mindsetCompleted',
                  'visionCompleted', 'purposeCompleted', 'seccion', 'usuario')


class UserPurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPurchase
        fields = ('originalTransactionId', 'transactionId', 'productId')


class PromotionalCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PromotionalCode
        fields = ('promotionalOffer',)


class StageSerializer(serializers.ModelSerializer):
    id_stage = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Stage
        fields = ('id_stage', 'image', 'title', 'order', 'minimum_days',
                  'fecha_creacion', 'fecha_modificacion')


class StageHabitoSerializer(serializers.ModelSerializer):
    id_stage_habito = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = StageHabito
        fields = ('id_stage_habito', 'id_stage', 'id_habito',
                  'synced', 'fecha_creacion', 'fecha_modificacion')


class BadgeSerializer(serializers.ModelSerializer):
    id_badge = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = Badge
        fields = ('id_badge', 'badge', 'title', 'order', 'minimum_days',
                  'fecha_creacion', 'fecha_modificacion')


class BadgeHabitoSerializer(serializers.ModelSerializer):
    id_badge_habito = serializers.CharField(default=str(uuid.uuid4()))

    class Meta:
        model = BadgeHabito
        fields = ('id_badge_habito', 'id_badge', 'id_habito',
                  'synced', 'fecha_creacion', 'fecha_modificacion')


class FocusOnPastOrFutureSerializer(serializers.ModelSerializer):
    class Meta:
        model = FocusOnPastOrFuture
        fields = ('dispositivo', 'respuesta', 'fecha')


class TurnYourLifeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TurnYourLife
        fields = ('dispositivo', 'respuesta', 'fecha')


class AffectsYouMostSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffectsYouMost
        fields = ('dispositivo', 'respuesta', 'fecha')
