from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ('email', 'first_name', 'last_name',)


class CustonUserChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = CustomUserCreationForm.Meta.fields
