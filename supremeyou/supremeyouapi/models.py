import uuid
import os
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db.models.base import Model
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django_s3_storage.storage import S3Storage
#from django_countries.fields import CountryField

# Create your models here.
storage = S3Storage(aws_s3_bucket_name=os.environ['AWS_STORAGE_BUCKET_NAME'])


class UsuarioManager(BaseUserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        # if not password:
        #    raise ValueError('Users must have a password')
        # if not extra_fields['first_name']:
        #    raise ValueError('Users must have a first name')
        # if not extra_fields.get('last_name'):
        #    raise ValueError('Users must have a last name')

        user = self.model(
            email=self.normalize_email(email),
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, first_name, last_name, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        if not password:
            raise ValueError('Users must have a password')
        if not first_name:
            raise ValueError('Users must have a first name')
        if not last_name:
            raise ValueError('Users must have a last name')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class Usuario(AbstractUser):
    username = models.CharField(
        blank=True, null=True, max_length=150, default='none')
    email = models.EmailField(_('email address'), unique=True)
    fecha_creacion = models.DateTimeField(
        auto_now_add=True, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UsuarioManager()

    def __str__(self):
        return "{}".format(self.email)


class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    current_weight = models.FloatField(default=0.0)
    ideal_weight = models.FloatField(default=0.0)
    current_income = models.FloatField(default=0.0)
    ideal_income = models.FloatField(default=0.0)
    education_level = models.IntegerField(default=0)
    religion = models.IntegerField(default=0)
    primary_occupation = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    children_number = models.IntegerField(default=0)
    child_athome = models.IntegerField(default=0)
    focusArea = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    hasMother = models.BooleanField(default=False)
    hasFather = models.BooleanField(default=False)
    hasSiblings = models.BooleanField(default=False)
    hasKids = models.BooleanField(default=False)
    relationship = models.IntegerField(default=-1)
    profileImage = models.ImageField(
        upload_to='profile_images', blank=True, null=True, storage=storage)
    motivationalQuote = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    personalVisionStatement = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    fecha_nacimiento = models.DateTimeField()
    wellnessDone = models.BooleanField(default=False)
    fourteenDayChallenge = models.BooleanField(default=False)
    #country = CountryField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)


class NorthStar(models.Model):
    id_north_star = models.AutoField(primary_key=True)
    desc_corta = models.CharField(max_length=50)
    desc_extendida = models.CharField(max_length=254)
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.desc_corta


class Nivel(models.Model):
    id_nivel = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    nivel = models.IntegerField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Nivel"
        verbose_name_plural = "Niveles"


class Etiqueta(models.Model):
    id_etiqueta = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre


class TipoMeditacion(models.Model):
    id_tipo_meditacion = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Tipo de Meditacion"
        verbose_name_plural = "Tipos de Meditaciones"


class TipoCategoria(models.Model):
    id_tipo_categoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Tipo de Categoria"
        verbose_name_plural = "Tipos de Categorias"


class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=35)
    id_tipo_categoria = models.ForeignKey(
        TipoCategoria, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)


class Recurso(models.Model):
    id_recurso = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=60)
    imagen = models.BinaryField(blank=True)
    video = models.BinaryField(blank=True)
    audio = models.BinaryField(blank=True)
    id_etiqueta = models.ForeignKey(Etiqueta, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo


class Meditacion(models.Model):
    id_meditacion = models.AutoField(primary_key=True)
    id_tipo_meditacion = models.ForeignKey(
        TipoMeditacion, on_delete=models.CASCADE)
    id_recurso = models.ForeignKey(Recurso, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Meditacion"
        verbose_name_plural = "Meditaciones"


class Task(models.Model):
    id_tarea = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    id_tarea_padre = models.ForeignKey(
        'self', on_delete=models.CASCADE, default=None, blank=True, null=True, related_name='%(class)s_tarea_padre', to_field='id_tarea')
    id_meta = models.ForeignKey(
        'Meta', on_delete=models.CASCADE, default=None, blank=True, null=True, related_name='%(class)s_meta', to_field='id_tarea')
    id_milestone = models.ForeignKey(
        'Milestone', on_delete=models.CASCADE, default=None, blank=True, null=True, related_name='%(class)s_milestone', to_field='id_tarea')
    id_lista = models.ForeignKey(
        'Lista', on_delete=models.CASCADE, default=None, blank=True, null=True, related_name='%(class)s_lista', to_field='id_lista')
    reminder = models.DateTimeField(default=None, blank=True, null=True)
    deadline = models.DateTimeField(default=None, blank=True, null=True)
    action_day = models.DateTimeField(default=None, blank=True, null=True)
    repeatDate = models.DateTimeField(default=None, blank=True, null=True)
    status = models.IntegerField(default=0)
    importancia = models.IntegerField(default=0)
    repeatFrecuency = models.IntegerField(default=0)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    actionDayId = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    reminderId = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    deadlineId = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    image = models.ImageField(upload_to='task_images',
                              blank=True, null=True, storage=storage)
    completed = models.BooleanField(default=False)
    position = models.IntegerField(default=0)
    isInMyDay = models.BooleanField(default=False)
    inMyDayDate = models.DateTimeField(default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)
    eliminado = models.BooleanField(default=False)
    deletedDate = models.DateTimeField(default=None, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre


class Tarea(Task):
    def __str__(self):
        return self.nombre


class RitualMatutino(models.Model):
    id_ritual_matutino = models.AutoField(primary_key=True)
    id_north_star = models.ForeignKey(NorthStar, on_delete=models.CASCADE)
    id_recurso = models.ForeignKey(Recurso, on_delete=models.CASCADE)
    id_meditacion = models.ForeignKey(Meditacion, on_delete=models.CASCADE)
    audio_exito = models.BinaryField(blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Ritual Matutino"
        verbose_name_plural = "Rituales Matutinos"


class RitualNoche(models.Model):
    id_ritual_noche = models.AutoField(primary_key=True)
    id_north_star = models.ForeignKey(NorthStar, on_delete=models.CASCADE)
    id_tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    audio_exito = models.BinaryField(blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Ritual Nocturno"
        verbose_name_plural = "Rituales Nocturnos"


class Diario(models.Model):
    id_diario = models.AutoField(primary_key=True)
    id_ritual_matutino = models.ForeignKey(
        RitualMatutino, on_delete=models.CASCADE)
    id_ritual_noche = models.ForeignKey(RitualNoche, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)


# Modelos para la encuesta inicial
class Seccion(models.Model):
    id_seccion = models.AutoField(primary_key=True)
    seccion = models.CharField(max_length=45)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.seccion

    class Meta:
        ordering = ["id_seccion"]
        verbose_name = "Seccion"
        verbose_name_plural = "Secciones"


class Preguntas(models.Model):
    id_pregunta = models.AutoField(primary_key=True)
    pregunta = models.CharField(max_length=1000)
    id_seccion = models.ForeignKey(Seccion, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.pregunta

    class Meta:
        verbose_name_plural = "Preguntas"
        verbose_name = "Pregunta"


class Encuesta(models.Model):
    id_encuesta = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    id_seccion = models.ForeignKey(Seccion, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.descripcion


class Cuestionario(models.Model):
    id_encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)
    id_preguntas = models.ForeignKey(Preguntas, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id_encuesta) + " + " + str(self.id_preguntas)


class Respuestas(models.Model):
    id_respuestas = models.AutoField(primary_key=True)
    respuesta_desc = models.CharField(max_length=45)
    valor = models.IntegerField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.respuesta_desc

    class Meta:
        verbose_name = "Respuesta Predefinida"
        verbose_name_plural = "Respuestas Predefinidas"


class RespuestaUsuario(models.Model):
    id_usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE)
    id_cuestionario = models.ForeignKey(
        Cuestionario, on_delete=models.CASCADE)
    id_respuestas = models.ForeignKey(Respuestas, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Respuesta del Usuario"
        verbose_name_plural = "Respuestas del Usuario"


class Relationships(models.Model):
    relacion = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Relación"
        verbose_name_plural = "Relaciones"

    def __str__(self):
        return self.relacion


class Careers(models.Model):
    career = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Carrera"
        verbose_name_plural = "Carreras"

    def __str__(self):
        return self.career


class Meta(Task):
    category = models.ForeignKey(
        Seccion, on_delete=models.CASCADE, default=None, blank=True, null=True, to_field='id_seccion')
    why = models.CharField(max_length=150, default=None, blank=True)
    isSupreme = models.BooleanField(default=False)
    isElemental = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre


class Milestone(Task):
    firstTime = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class Habito(Task):
    what_triggers = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    what_desire = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    what_do = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    reward = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    good_action = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    notificationLabel = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    remind = models.BooleanField(default=False)
    pre15 = models.BooleanField(default=False)
    pre20 = models.BooleanField(default=False)
    pre1h = models.BooleanField(default=False)
    main_goal = models.ForeignKey(
        Meta, on_delete=models.CASCADE, default=None, blank=True, null=True)
    frecuencySetDate = models.DateTimeField(
        default=None, blank=True, null=True)
    whatTime = models.DateTimeField(default=None, blank=True, null=True)
    isAnyTime = models.BooleanField(default=False)

    def __str__(self):
        return self.good_action


class HabitFrecuencyDays(models.Model):
    id_habit_frecuency_days = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    day = models.IntegerField(default=None, blank=True, null=True)
    month = models.IntegerField(default=None, blank=True, null=True)
    year = models.IntegerField(default=None, blank=True, null=True)
    weekday = models.IntegerField(default=None, blank=True, null=True)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)


class HabitAlarm(models.Model):
    id_habit_alarm = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    hour = models.IntegerField(default=0)
    minute = models.IntegerField(default=0)
    weekday = models.IntegerField(default=0)
    day = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
    label = models.CharField(max_length=150)
    notificationId = models.CharField(max_length=150)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)


class HabitoCompletado(models.Model):
    id_habito_completado = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    completedDate = models.DateTimeField()
    completado = models.BooleanField(default=False)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)


class Trigger(models.Model):
    id_trigger = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    label = models.CharField(max_length=150)
    time = models.DateTimeField()
    soundId = models.IntegerField()
    isEnabled = models.BooleanField(default=False)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self):
        return self.label


class TriggerEvent(models.Model):
    id_trigger_event = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    eventId = models.CharField(
        max_length=150, default=None, blank=True, null=True)
    trigger = models.ForeignKey(
        Trigger, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)


class Reward(models.Model):
    id_reward = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=150)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True, related_name='%(class)s_habito')
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Ability(models.Model):
    id_ability = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=150)
    habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Nota(models.Model):
    id_nota = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    nota = models.CharField(max_length=200, default=None)
    tarea = models.ForeignKey(
        Tarea, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)
    eliminado = models.BooleanField(default=False)
    deletedDate = models.DateTimeField(default=None, blank=True, null=True)

    def __str__(self):
        return self.nota


class Lista(models.Model):
    id_lista = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=150)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre


class SyncInfo(models.Model):
    synced_date = models.DateTimeField()

    def __str__(self):
        return str(self.synced_date)


class TenAreas(models.Model):
    id_ten_areas = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    seccion = models.ForeignKey(
        Seccion, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    introCompleted = models.BooleanField(default=False)
    mindsetCompleted = models.BooleanField(default=False)
    visionCompleted = models.BooleanField(default=False)
    purposeCompleted = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.seccion.seccion

    class Meta:
        ordering = ["fecha_creacion"]
        verbose_name = "Ten Areas"
        verbose_name_plural = "Ten Areas"


class UserPurchase(models.Model):
    originalTransactionId = models.CharField(max_length=150)
    transactionId = models.CharField(max_length=150, primary_key=True)
    productId = models.CharField(max_length=150)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.originalTransactionId

    class Meta:
        ordering = ["fecha_creacion"]
        verbose_name = "User Purchase"
        verbose_name_plural = "User Purchases"


class PromotionalCode(models.Model):
    promotionalOffer = models.CharField(max_length=150)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.promotionalOffer

    class Meta:
        ordering = ["fecha_creacion"]
        verbose_name = "Promotional Code"
        verbose_name_plural = "Promotional Codes"


class Stage(models.Model):
    id_stage = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    image = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    order = models.IntegerField(default=0)
    minimum_days = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title


class StageHabito(models.Model):
    id_stage_habito = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    id_stage = models.ForeignKey(
        Stage, on_delete=models.CASCADE, default=None, blank=True, null=True)
    id_habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self) -> str:
        return '{}+{}'.format(self.id_stage.title, self.id_habito.good_action)


class Badge(models.Model):
    id_badge = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    badge = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    order = models.IntegerField(default=0)
    minimum_days = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title


class BadgeHabito(models.Model):
    id_badge_habito = models.CharField(
        max_length=150, primary_key=True, default=uuid.uuid4, editable=False)
    id_badge = models.ForeignKey(
        Stage, on_delete=models.CASCADE, default=None, blank=True, null=True)
    id_habito = models.ForeignKey(
        Habito, on_delete=models.CASCADE, default=None, blank=True, null=True)
    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, default=None, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    synced = models.BooleanField(default=False)

    def __str__(self) -> str:
        return '{}+{}'.format(self.id_badge.title, self.id_habito.good_action)


class FocusOnPastOrFuture(models.Model):
    id = models.AutoField(primary_key=True)
    dispositivo = models.CharField(max_length=255)
    respuesta = models.CharField(max_length=255)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return "{} + {}".format(self.dispositivo, self.respuesta)

    class Meta:
        verbose_name = 'Focus on past or future Answer'
        verbose_name_plural = 'Focus on past or future Answers'


class TurnYourLife(models.Model):
    id = models.AutoField(primary_key=True)
    dispositivo = models.CharField(max_length=255)
    respuesta = models.CharField(max_length=255)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return "{} + {}".format(self.dispositivo, self.respuesta)

    class Meta:
        verbose_name = 'Turn your life Answer'
        verbose_name_plural = 'Turn your life Answers'


class AffectsYouMost(models.Model):
    id = models.AutoField(primary_key=True)
    dispositivo = models.CharField(max_length=255)
    respuesta = models.CharField(max_length=255)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return "{} + {}".format(self.dispositivo, self.respuesta)

    class Meta:
        verbose_name = 'Affects you most Answer'
        verbose_name_plural = 'Affects you most Answers'
