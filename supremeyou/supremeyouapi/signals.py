from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from .models import UserProfile
from mailchimp_marketing import Client
from mailchimp_marketing.api_client import ApiClientError
from datetime import datetime


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_profile_handler(sender, instance, created, **kwargs):
    if not created:
        return

    if not UserProfile.objects.filter(user=instance).exists():
        profile_data = {}
        profile_data['fecha_nacimiento'] = datetime.now()
        UserProfile.objects.create(user=instance, **profile_data)

    try:
        mailchimp = Client()
        mailchimp.set_config({
            "api_key": "897cede75f18c299dfc1f79578014862-us20",
            "server": "us20"
        })

        response = mailchimp.lists.add_list_member(
            "12217b90b5", {"email_address": instance.email, "status": "subscribed", "merge_fields": {"FNAME": instance.first_name}})
        print(response)
    except ApiClientError as error:
        print("Error {}".format(error.text))
