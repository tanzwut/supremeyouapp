import csv
from django.contrib import admin
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.http import HttpResponse

from .models import AffectsYouMost, Badge, BadgeHabito, FocusOnPastOrFuture, PromotionalCode, Stage, StageHabito, TenAreas, TurnYourLife, UserPurchase, Usuario, UserProfile, Categoria, Diario, Etiqueta, Meditacion, Meta, Nivel, NorthStar, Nota, Recurso, RitualMatutino, RitualNoche, Tarea, TipoCategoria, TipoMeditacion, Seccion, Preguntas, Encuesta, Cuestionario, Respuestas, RespuestaUsuario, Relationships, Careers, Milestone, Habito, HabitFrecuencyDays, HabitoCompletado, HabitAlarm, Trigger, TriggerEvent, Reward, Ability, Lista, SyncInfo


def download_csv(self, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=users.csv'
    writer = csv.writer(response)
    writer.writerow(["email", "first_name"])
    for s in queryset:
        writer.writerow([s.email, s.first_name])

    return response


download_csv.short_description = 'Export selected'


class UserprofileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


@admin.register(Usuario)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff',
                                       'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {'classes': ('wide',), 'fields': (
            'email', 'password1', 'password2')}),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('first_name',)
    inlines = (UserprofileInline,)
    actions = [download_csv]


class TenAreasAdmin(admin.ModelAdmin):
    fields = ('introCompleted', 'mindsetCompleted', 'visionCompleted',
              'purposeCompleted', 'seccion', 'usuario')
    list_display = ('seccion', 'introCompleted', 'mindsetCompleted',
                    'visionCompleted', 'purposeCompleted', 'usuario')
    list_filter = ('introCompleted', 'mindsetCompleted',
                   'visionCompleted', 'purposeCompleted', 'seccion', 'usuario')
    search_fields = ['usuario__email']


class UserPurchaseAdmin(admin.ModelAdmin):
    fields = ('originalTransactionId', 'transactionId',
              'productId', 'usuario')
    list_display = ('originalTransactionId', 'transactionId',
                    'productId', 'usuario', 'fecha_creacion')
    list_filter = ('originalTransactionId', 'transactionId',
                   'productId', 'usuario', 'fecha_creacion')
    search_fields = ['originalTransactionId', 'transactionId',
                     'productId', 'usuario__email']


class PromotionalCodeAdmin(admin.ModelAdmin):
    fields = ('promotionalOffer',
              'usuario')
    list_display = ('promotionalOffer', 'usuario', 'fecha_creacion')
    list_filter = ('promotionalOffer', 'usuario', 'fecha_creacion')
    search_fields = ['promotionalOffer', 'usuario__email']


class StageAdmin(admin.ModelAdmin):
    fields = ('image', 'title', 'order', 'minimum_days')
    list_display = ('image', 'title', 'order', 'minimum_days',
                    'fecha_creacion', 'fecha_modificacion')
    list_filter = ('image', 'title', 'order', 'minimum_days',
                   'fecha_creacion', 'fecha_modificacion')
    search_fields = ['title']


class StageHabitoAdmin(admin.ModelAdmin):
    fields = ('id_stage', 'id_habito', 'synced')
    list_display = ('id_stage', 'id_habito',
                    'synced', 'fecha_creacion', 'fecha_modificacion')
    list_filter = ('id_stage', 'id_habito',
                   'synced', 'fecha_creacion', 'fecha_modificacion')
    search_fields = ['id_stage', 'id_habito']


class BadgeAdmin(admin.ModelAdmin):
    fields = ('badge', 'title', 'order', 'minimum_days')
    list_display = ('badge', 'title', 'order', 'minimum_days',
                    'fecha_creacion', 'fecha_modificacion')
    list_filter = ('badge', 'title', 'order', 'minimum_days',
                   'fecha_creacion', 'fecha_modificacion')
    search_fields = ['title']


class BadgeHabitoAdmin(admin.ModelAdmin):
    fields = ('id_badge', 'id_habito', 'synced')
    list_display = ('id_badge', 'id_habito',
                    'synced', 'fecha_creacion', 'fecha_modificacion')
    list_filter = ('id_badge', 'id_habito',
                   'synced', 'fecha_creacion', 'fecha_modificacion')
    search_fields = ['id_badge', 'id_habito']


class FocusOnPastOrFutureAdmin(admin.ModelAdmin):
    fields = ('dispositivo', 'respuesta')
    list_display = ('dispositivo', 'respuesta', 'fecha')
    list_filter = ('dispositivo', 'respuesta', 'fecha')
    search_fields = ['dispositivo', 'respuesta', 'fecha']


class TurnYourLifeAdmin(admin.ModelAdmin):
    fields = ('dispositivo', 'respuesta')
    list_display = ('dispositivo', 'respuesta', 'fecha')
    list_filter = ('dispositivo', 'respuesta', 'fecha')
    search_fields = ['dispositivo', 'respuesta', 'fecha']


class AffectsYouMostAdmin(admin.ModelAdmin):
    fields = ('dispositivo', 'respuesta')
    list_display = ('dispositivo', 'respuesta', 'fecha')
    list_filter = ('dispositivo', 'respuesta', 'fecha')
    search_fields = ['dispositivo', 'respuesta', 'fecha']


# Register your models here.
admin.site.register(Categoria)
admin.site.register(Diario)
admin.site.register(Etiqueta)
admin.site.register(Meditacion)
admin.site.register(Meta)
admin.site.register(Nivel)
admin.site.register(NorthStar)
admin.site.register(Nota)
admin.site.register(Recurso)
admin.site.register(RitualMatutino)
admin.site.register(RitualNoche)
admin.site.register(Tarea)
admin.site.register(TipoCategoria)
admin.site.register(TipoMeditacion)
admin.site.register(Seccion)
admin.site.register(Encuesta)
admin.site.register(Cuestionario)
admin.site.register(Preguntas)
admin.site.register(RespuestaUsuario)
admin.site.register(Respuestas)
admin.site.register(Relationships)
admin.site.register(Careers)
admin.site.unregister(Group)
admin.site.register(Milestone)
admin.site.register(Habito)
admin.site.register(HabitFrecuencyDays)
admin.site.register(HabitoCompletado)
admin.site.register(HabitAlarm)
admin.site.register(Trigger)
admin.site.register(TriggerEvent)
admin.site.register(Reward)
admin.site.register(Ability)
admin.site.register(Lista)
admin.site.register(SyncInfo)
admin.site.register(TenAreas, TenAreasAdmin)
admin.site.register(UserPurchase, UserPurchaseAdmin)
admin.site.register(PromotionalCode, PromotionalCodeAdmin)
admin.site.register(Stage, StageAdmin)
admin.site.register(StageHabito, StageHabitoAdmin)
admin.site.register(Badge, BadgeAdmin)
admin.site.register(BadgeHabito, BadgeHabitoAdmin)
admin.site.register(FocusOnPastOrFuture, FocusOnPastOrFutureAdmin)
admin.site.register(TurnYourLife, TurnYourLifeAdmin)
admin.site.register(AffectsYouMost, AffectsYouMostAdmin)
