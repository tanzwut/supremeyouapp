
/*$(document).ready(function () {
    var input = document.getElementById("newPassword1");
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length");

    $(document).on('keyup', '#newPassword1', function (e) {
        var lowerCaseLetters = /[a-z]/g;
        if (input.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (input.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (input.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
        }

        // Validate length
        if (input.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
        }
    });
});

$(document).on('focus', '#newPassword1', function (e) {
    console.log("enter focus")
    document.getElementById('message').style.display = "block";
});

$(document).on('focusout', '#newPassword1', function (e) {
    document.getElementById('message').style.display = "none";
});*/

$(document).on('submit', '#post-form', function (event) {
    event.preventDefault();
    var newPassword1 = document.getElementById('newPassword1').value;
    var newPassword2 = document.getElementById('newPassword2').value;
    var alertError = document.getElementById('alertError');
    var alertSuccess = document.getElementById('alertSuccess');

    if (alertError) {
        document.body.removeChild(alertError);
    }

    if (alertSuccess) {
        document.body.removeChild(alertSuccess);
    }

    if (newPassword1 && newPassword2) {
        var passwordVerifyResponse = verifyPassword(newPassword1, newPassword2);
        if (passwordVerifyResponse === "") {
            /*$.ajax({
                type: 'POST',
                url: 'https://www.supremeyou.co/app/api/v1/password_reset/confirm/',
                data: {
                    action: 'POST',
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
                    token: '{{ token }}'
                },
                success: function (json) {
                    document.getElementById("post-form").reset();
                    //console.log(json);
                },
                error: function (xhr, errmsg, err) {
                    console.log(xhr.status + ": " + xhr.responseText);
                    var node = document.createElement("div");
                    node.classList.add("alert");
                    node.classList.add("alert-danger");
                    node.id = 'alertError';
                    node.setAttribute("role", "alert");
                    var textNode = document.createTextNode(xhr.);
                    node.appendChild(textNode);

                    document.body.appendChild(node);
                }
            })*/

            const urlParams = new URLSearchParams(window.location.search);
            console.log(urlParams.get('token'));

            const csrfCookie = getCookie('csrftoken');

            fetch('https://www.supremeyou.co/app/api/v1/password_reset/confirm/', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRFToken': csrfCookie
                },
                body: JSON.stringify({
                    token: urlParams.get('token'),
                    password: newPassword2
                })
            })
                .then(response => response.json())
                .then(response => {
                    if (response.hasOwnProperty('token')) {
                        var keys = Object.keys(response)
                        var key = keys.find(element => element === 'token')
                        showErrorAlert(key + ": " + response.token)
                    }

                    else if (response.hasOwnProperty('password')) {
                        var keys = Object.keys(response)
                        var key = keys.find(element => element === 'password')
                        showErrorAlert(key + ": " + response.password)
                    }
                    else {
                        if (response.hasOwnProperty('status')) {
                            if (response.status === 'OK') {
                                showSuccessAlert('The password has been changed.')
                                document.getElementById("post-form").reset();
                            }
                            else {
                                showErrorAlert(response.status)
                            }
                        }
                    }
                })
                .catch(error => console.log(error));
        }
        else {
            console.log("Las Contraseñas no coinciden")
            showErrorAlert(passwordVerifyResponse)
        }
    }
    else {
        console.log("One or more password fields are empty")
        showErrorAlert("One or more password fields are empty")
    }
});

function verifyPassword(password1, password2) {

    if (password1.length < 8) {
        return "Password must be 8 characters min "
    }
    else if (password1 !== password2) {
        return "The two passwords doesn't match "
    }
    else {
        return ""
    }
}

function showErrorAlert(text) {
    var node = document.createElement("div");
    node.classList.add("alert");
    node.classList.add("alert-danger");
    node.id = 'alertError';
    node.setAttribute("role", "alert");
    var textNode = document.createTextNode(text);
    node.appendChild(textNode);

    document.body.appendChild(node);
}

function showSuccessAlert(text) {
    var node = document.createElement("div");
    node.classList.add("alert");
    node.classList.add("alert-success");
    node.id = 'alertSuccess';
    node.setAttribute("role", "alert");
    var textNode = document.createTextNode(text);
    node.appendChild(textNode);

    document.body.appendChild(node);
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}