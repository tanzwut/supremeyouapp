"""supremeyou URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

from supremeyouapi import views
from supremeyouapi.views import HomePageView

urlpatterns = [
    path('', views.home_view, name='home'),
    #path('signup/', SignUp.as_view(), name='signup'),
    path('videos/<str:video>', views.awarness_video_view, name='awareness'),
    path('face2ios/', views.face_to_ios, name='face2ios'),

    # Django admin
    path('admin/', admin.site.urls),

    # SupremeYouAPI management
    path('users/', include('django.contrib.auth.urls')),
    path('app/api/v1/', include('supremeyouapi.urls')),
    path('accounts/', include('allauth.urls')),
    url(r'^auth/', include('drf_social_oauth2.urls', namespace='drf')),
]
